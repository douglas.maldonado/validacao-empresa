package br.com.bureau.validacao.services;

import br.com.bureau.cadastro.models.Empresa;
import br.com.bureau.validacao.clients.ReceitaClient;
import br.com.bureau.validacao.models.Receita;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ValidaService {

    @Autowired
    private KafkaTemplate<String, Empresa> producer;

    @Autowired
    private ReceitaClient receitaClient;

    public void chamarReceita(Empresa empresa){
        try{
            Optional<Receita> optionalReceita = receitaClient.consultaCNPJ(empresa.getCnpj());

            if(optionalReceita.isPresent()){
                if(optionalReceita.get().getCapital_social().doubleValue() > 1000000.00) {
                    enviarKafka(empresa);
                }
            }

        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    private void enviarKafka(Empresa empresa) {
        producer.send("spec2-douglas-maldonado-3", empresa);
    }
}
