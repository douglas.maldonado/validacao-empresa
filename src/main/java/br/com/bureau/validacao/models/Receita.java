package br.com.bureau.validacao.models;

import java.math.BigDecimal;

public class Receita {

    public BigDecimal capital_social;

    public Receita() {
    }

    public BigDecimal getCapital_social() {
        return capital_social;
    }

    public void setCapital_social(BigDecimal capital_social) {
        this.capital_social = capital_social;
    }
}
