package br.com.bureau.validacao.consumers;

import br.com.bureau.cadastro.models.Empresa;
import br.com.bureau.validacao.services.ValidaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class EmpresaConsumer {

    @Autowired
    private ValidaService validaService;

    @KafkaListener(topics = "spec3-douglas-maldoando-2", groupId = "douglas-1")
    public void receber(@Payload Empresa empresa){

        validaService.chamarReceita(empresa);

    }
}
